import mongoose, { Schema } from 'mongoose';

export interface ProductInterface {
    name: string,
    category: Array<string>
  }

const productScheme = new Schema({
    name: {
        type: String,
        required: true,
        minlength: 1,
        maxlength: 30,
        default: "noname"
    },
    category: [{
        type: mongoose.Schema.Types.ObjectId, ref: "Category",
       
    }]
},
    { versionKey: false }
);
const Product = mongoose.model<ProductInterface>("Product", productScheme);
module.exports = Product;
