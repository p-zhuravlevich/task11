import mongoose, { Schema } from 'mongoose';

export interface CategoryInterface {
  name: string,
  products: Array<string>
}

const categoryScheme = new Schema({
    name: {
        type: String,
        required: true,
        minlength:1,
        maxlength:20,
        default:"noname"
    },
    products :[{
        type: mongoose.Schema.Types.ObjectId, ref:"Product",
    }]
},
{versionKey: false }
);

const Category = mongoose.model<CategoryInterface>("Category", categoryScheme);
module.exports = Category;
