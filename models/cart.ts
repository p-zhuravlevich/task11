import mongoose, { Schema } from 'mongoose';

export interface CartInterface {
    user: string,
    products: Array<string>,
  }

const cartScheme = new Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId, ref: "User",
        
    },
    products: [{
        type: mongoose.Schema.Types.ObjectId, ref: "Product",
    }]
},
    { versionKey: false }
);

const Cart = mongoose.model<CartInterface>("Cart", cartScheme);
module.exports = Cart;
