import { Router } from 'express';
import { CategoryController }  from '../controllers/categoryController';
import { Auth }  from '../auth';

const categoryRout: Router = Router();

categoryRout.post("/create",Auth.verifyToken,Auth.isAdmin,CategoryController.createCategory);
categoryRout.delete("/:id",Auth.verifyToken,Auth.isAdmin, CategoryController.deleteCategory);
categoryRout.put("/:id", Auth.verifyToken,Auth.isAdmin, CategoryController.updateCategory);
categoryRout.get("/:id", Auth.verifyToken,CategoryController.getСategoryById);
categoryRout.get("/get/all", Auth.verifyToken,CategoryController.getAllСategories);

export default categoryRout;
