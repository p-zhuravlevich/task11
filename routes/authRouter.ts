import { Router } from 'express';
import { AuthController }  from '../controllers/authController';
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({extended: true});

const authRouter: Router = Router();

authRouter.post("/register", urlencodedParser, AuthController.regUser);
authRouter.post("/login", urlencodedParser, AuthController.loginUser);
authRouter.get("/auth", AuthController.googleLogin);
authRouter.get("/googleauth/", AuthController.googleAuth);
 
export default authRouter;