import { Router } from 'express';
import { UserController }  from '../controllers/userController';
import { Auth }  from '../auth';
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({extended: true});

const userRouter: Router = Router();

userRouter.post("/create", urlencodedParser, Auth.verifyToken, Auth.isAdmin, UserController.createUser);
userRouter.delete("/delete/:id", urlencodedParser, Auth.verifyToken, Auth.isAdmin, UserController.deleteUser);
userRouter.put("/update", urlencodedParser, Auth.verifyToken, UserController.updateUser);
userRouter.get("/getbyid/:id", urlencodedParser, Auth.verifyToken, UserController.getUserByID);
userRouter.get("/getallusers", urlencodedParser, Auth.verifyToken, UserController.getAllUsers);

 
export default userRouter;