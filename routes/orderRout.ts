import { Router } from 'express';
import { OrderController }  from '../controllers/orderController';
import { Auth }  from '../auth';

const orderRout: Router = Router();

orderRout.post("/create", Auth.verifyToken, OrderController.createOrder);
orderRout.delete("/:id", Auth.verifyToken, OrderController.deleteOrder);
orderRout.get("/:id", Auth.verifyToken, OrderController.getOrderById);
orderRout.get("/get", Auth.verifyToken, OrderController.getAllOrders);

export default orderRout;
