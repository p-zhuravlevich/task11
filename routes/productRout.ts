import { Router } from 'express';
import { ProductController }  from '../controllers/productController';
import { Auth }  from '../auth';


const productRout: Router = Router();

productRout.post("/create", Auth.verifyToken, Auth.isAdmin ,ProductController.createProduct);
productRout.delete("/:id", Auth.verifyToken, Auth.isAdmin, ProductController.deleteProduct);
productRout.put("/:id", Auth.verifyToken, Auth.isAdmin, ProductController.updateProduct);
productRout.get("/:id", Auth.verifyToken, ProductController.getProductById);
productRout.get("/get/all", Auth.verifyToken, ProductController.getAllProducts);

export default productRout;
