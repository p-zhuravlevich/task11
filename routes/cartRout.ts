import { Router } from 'express';
import { CartController }  from '../controllers/cartController';
import { Auth }  from '../auth';

const cartRout: Router = Router();


cartRout.post("/create", Auth.verifyToken, CartController.createCart);
cartRout.get("/:id", Auth.verifyToken, CartController.getCartById);
cartRout.put("/add", Auth.verifyToken, CartController.addProductToCart);

export default cartRout;
