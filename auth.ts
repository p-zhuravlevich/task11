// import { UserInterface } from "../models/user";
import { Request, Response, NextFunction } from "express";

const jwt = require('jsonwebtoken');
const accessToken = require('./config');
const User = require('./models/user');

export class Auth{
  public static async verifyToken(req: Request, res: Response, next: NextFunction): Promise<void> {
    const authHeader: any = req.headers.authorization;
    const token = authHeader.split(' ')[1];
    if (!token) {
      res.status(403).send("A token is required for authentication");
    }
    try {
      const decoded = jwt.verify(token, accessToken);;
      (req as any).user = decoded;
      (req as any).decoded = decoded;
    } catch (err) {
      res.status(401).send("Invalid Token");
    }
    return next();
  };
  
  public static async  isAdmin(req: Request, res: Response, next: NextFunction): Promise<void>{
      const admin = await User.findOne({_id: (req as any).decoded.user_id});
    if (admin.role !== "Admin") {
      res.status(403).send("Forbidden. Ask for Admin")
    }
    return next();
  }
};