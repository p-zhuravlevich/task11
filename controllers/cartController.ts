const Cart = require('../models/cart');
const User = require('../models/user');
const Product = require('../models/product');
import { CartInterface } from "../models/cart";
import { UserInterface } from "../models/user";
import { ProductInterface } from "../models/product";
import { Request, Response } from "express";

interface NewRequest<data> extends Request{
    body: data,
}


export class CartController{
    public static async createCart(req: NewRequest<UserInterface>, res: Response): Promise<void> {
        try {
            let user = await User.findOne({ _id: (req as any).decoded.user_id });
            if (user.cart) {
                res.status(400).send("У вас уже есть одна корзина")
            }
            else {
                const newCart = await new Cart({});
                newCart.user = (req as any).decoded.user_id;
                await newCart.save();
                user.cart = newCart._id;
                await user.save();
                res.send(newCart);
            }
        }
        catch (err) {
            res.status(400).send("Что-то пошло не так")
        }
    }
    
    public static async getCartById(req: NewRequest<CartInterface>, res: Response): Promise<void> {
        try {
            let cart = await Cart.findOne({ _id: req.params.id });
            if (cart) {
                res.send(`корзина: ${cart}`)
            }
            else {
                res.send("Такой корзины нет")
            }
        }
        catch (err) {
            res.status(400).send("Что-то пошло не так")
        }
    }
    
    public static async addProductToCart(req: NewRequest<ProductInterface>, res: Response): Promise<void> {
        try {
            let cartUser = await User.findOne({ _id: (req as any).decoded.user_id });
            let cart = await Cart.findOne({ _id: cartUser.cart._id });
            let product = await Product.findOne({ name: req.body.name });
    
            if (!cart) {
                res.send("У вас нет корзины");
            } else if (!product) {
                res.send("Такого товара не существует")
            }
            else if (product && cart) {
                cart.products.push(product._id.toString());
                await cart.save();
                res.send(`Товар ${product.name} добавлен в корзину пользователя ${cartUser.name}`)
            }
            else {
                res.send("Такой корзины нет")
            }
        }
        catch (err) {
            res.status(400).send("Что-то пошло не так")
        }
    }
}


