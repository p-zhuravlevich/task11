import { ProductInterface } from "../models/product";
// import { CategoryInterface } from "../models/category";
import { Request, Response } from "express";
const Category = require('../models/category');
const Product = require('../models/product');

interface NewRequest<productData> extends Request{
    body: productData
}

export class ProductController{
    public static async createProduct(req: NewRequest<ProductInterface>, res: Response): Promise<void> {
        try {
            let product = await Product.findOne({ name: req.body.name });
    
            let category = await Category.findOne({ name: req.body.category });
    
            if (!category) {
                res.send("Такой категории не существует")
            }
    
           else if (product) {
                res.status(400).send("Такой товар уже существует")
            }
            
            else
            {
                const newProduct = await new Product({ name: req.body.name});
                newProduct.category.push(category._id)
                await newProduct.save();
                category.products.push(newProduct._id);
                await category.save();
                res.send(`Новый товар создан: ${newProduct.name}`);
            }
        }
        catch (err) {
            res.status(400).send("Что-то пошло не так")
        }
    }
    
    public static async deleteProduct(req: NewRequest<ProductInterface>, res: Response): Promise<void> {
        try {
            let product = await Product.findOne({ _id: req.params.id });
            let category = await Category.findOne({ _id: product.category });
            if (product) {
                Product.deleteOne({ _id: req.params.id });
                for (let i = 0; i < category.products.length; i++) {
                    if (category.products[i]._id.toString() == product._id.toString()) {
                        category.products.splice(category.products.indexOf(i), 1);
                        await category.save()
                    }
                }
                res.send(`Удалён товар ${product.name}`);
            }
            else {
                res.send("Такого товара нет");
            }
        }
        catch (err) {
            res.status(400).send("Что-то пошло нет так")
        }
    }
    
    public static async updateProduct(req: NewRequest<ProductInterface>, res: Response): Promise<void> {
        try {
            let product = await Product.findOne({ _id: req.params.id });
            if (product) {
                Product.updateOne({ _id: req.params.id }, { name: req.body.name });
                res.send(`Товар изменён ${product.name}`);
            }
            else {
                res.send("Такого товара нет");
            }
        }
        catch (err) {
            res.status(400).send("Что-то пошло не так")
        }
    }
    
    public static async getProductById(req: Request, res: Response): Promise<void> {
        try {
            let product = await Product.findOne({ _id: req.params.id });
            if (product) {
                res.send(`Товар: ${product.name}`)
            }
            else {
                res.send("Такого товара нет")
            }
        }
        catch (err) {
            res.status(400).send("Что-то пошло не так")
        }
    }
    
    public static async getAllProducts(req: Request, res: Response): Promise<void> {
        try {
            let products = await Product.find();
            if (products) {
                res.send(`${products}`)
            }
            else {
                res.send("Товары отсутствуют")
            }
        }
        catch (err) {
            res.status(400).send("Что-то пошло не так")
        }
    }
}

