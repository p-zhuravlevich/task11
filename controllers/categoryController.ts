import { CategoryInterface } from "../models/category";
import { Request, Response } from "express";
const Category = require('../models/category');

interface NewRequest<data> extends Request{
    body: data
  }

export class CategoryController {
    public static async createCategory(req: NewRequest<CategoryInterface>, res: Response): Promise<void> {
        try {
            let category = await Category.findOne({ name: req.body.name });
            if (category) {
                res.status(400).send("Такая категория уже существует")
            }
            else {
                const newCategory = new Category({ name: req.body.name});
                await newCategory.save();
                res.send(`Новая категория создана: ${newCategory.name}`);
            }
        }
        catch (err) {
            res.status(400).send("Что-то пошло не так")
        }
    };
    
    public static async deleteCategory(req: NewRequest<CategoryInterface>, res: Response): Promise<void> {
        try {
            let category = await Category.findOne({_id: req.params.id});
            if (category) {
                Category.deleteOne({ _id: req.params.id });
                res.send(`Удалена категория ${category.name}`);
            }
            else {
                res.send("Такой категории нет");
            }
        }
        catch (err) {
            res.status(400).send("Что-то пошло нет так")
        }
    };
    
    public static async updateCategory(req: NewRequest<CategoryInterface>, res: Response): Promise<void> {
        try {
            let category = await Category.findOne({ _id: req.params.id });
            if (category) {
                Category.updateOne({ _id: req.params.id }, {name:req.body.name});
                let categoryNew = await Category.findOne({ _id: req.params.id });
                let newName = categoryNew.name
                res.send(`Категория ${category.name} изменена на ${newName}`);
            }
            else {
                res.send("Такой категории нет");
            }
        }
        catch (err) {
            res.status(400).send("Что-то пошло не так")
        }
    };
    
    public static async getСategoryById(req: Request, res: Response): Promise<void> {
        try {
            let category = await  Category.findOne({ _id: req.params.id });
            if (category) {
                res.send(`Категория: ${category.name}`)
            }
            else {
                res.send("Такой категории нет")
            }
        }
        catch (err) {
            res.status(400).send("Что-то пошло не так")
        }
    };
    
    public static async getAllСategories(req: Request, res: Response): Promise<void> {
        try {
            let categories = await Category.find();
            if (categories) {
                res.send(`${categories}`)
            }
            else {
                res.send("Категорий не существует")
            }
        }
        catch (err) {
            res.status(400).send("Что-то пошло не так")
        }
    };
}


