import { UserInterface } from "../models/user";
import { Request, Response } from "express";

const User = require('../models/user')
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const accessToken = require('../config');
const googleClientId = '858074835463-03l01lh3a6luc8817o7534n1d3kka1j4.apps.googleusercontent.com';
const googleSecret = "PuYPeUN1X0eztgmHpbk8Trtr";
const googleRedirect = 'http://localhost:3000/googleauth';
const {google} = require('googleapis');
const urlParse = require('url-parse'); 
const queryString = require('query-string');
const axios = require('axios');

const oauth2Client = new google.auth.OAuth2(
  googleClientId,
  googleSecret,
  googleRedirect);

const url = oauth2Client.generateAuthUrl({
  access_type: 'offline',
  scope: ['https://www.googleapis.com/auth/userinfo.profile',
          'https://www.googleapis.com/auth/userinfo.email'
  ]
});

interface NewRequest<data> extends Request{
  body: data
}

export class AuthController {
  public static async regUser(req: NewRequest<UserInterface>, res: Response): Promise<void>{
    try{
      const { name, surname, email, password } = req.body;
    if (!(email && password && surname && name)) {
      res.status(400).send("All input is required");
    }
    const oldUser = await User.findOne({ email });
    if (oldUser) {
      res.status(409).send("User Already Exist. Please email");
    }
    let role = req.body.role;
    let encryptedPassword = await bcrypt.hash(password, 10);
    const user = await User.create({
      name,
      surname,
      email: email.toLowerCase(), 
      password: encryptedPassword,
      role: role,
    });
    const token = jwt.sign(
      { user_id: user._id, email },
        accessToken,
      {
        expiresIn: "20h",
      }
    );
    user.token = token;
    res.status(201).json(user);
  } catch(err){
    console.log(err);
    res.status(400).send('Create user error');
    }
  }

  public static async loginUser(req: NewRequest<UserInterface>, res: Response): Promise<void>{
    try{
      const { email, password } = req.body;
    if (!(email && password)) {
      res.status(400).send("All input is required");
    }
    const user = await User.findOne({ email });
    if (user && (await bcrypt.compare(password, user.password))) {
      const token = jwt.sign(
        { user_id: user._id, email },
          accessToken,
        {
          expiresIn: "20h",
        }
      );
      user.token = token;
      res.status(200).json(user);
    }
    res.status(400).send("Try to use other login or password for log in.");
    }
    catch (err){
      console.log(err);
      res.status(400).send('Login user error')
    }
  }

  public static async googleLogin(req: Request, res: Response): Promise<void>{
    res.send(url)
  }

  public static async googleAuth(req: NewRequest<UserInterface>, res: Response): Promise<void>{
    try{
      const googleUrl = new urlParse(req.url);
      const code = queryString.parse(googleUrl.query).code;
      const {tokens} = await oauth2Client.getToken(code);
  
      const {data} = await axios({
        url: 'https://www.googleapis.com/oauth2/v2/userinfo',
        method: 'get',
        headers: {
          authorization: `Bearer ${tokens.access_token}`
        }
      })
  
      const user = await User.findOne({email: data.email});
      if(user){
        const accessTokenForUser = jwt.sign({
          name: user.name, 
          role: user.role, 
          _id: user._id}, 
          accessToken, 
          {expiresIn: '25h'});
        res.send(accessTokenForUser)
      } else {
        const createUser = new User({
          name: data.given_name, 
          surname: data.family_name, 
          email: data.email});
        await createUser.save();
        const accessTokenForUser = jwt.sign({
          name: createUser.name, 
          role: createUser.role, 
          _id: createUser._id}, 
          accessToken, 
          {expiresIn: '25h'});
        res.send(`${createUser} ${accessTokenForUser}`)
      }
    }catch (err){
      res.status(400).send('Google Auth Error')
    }
  }
}
