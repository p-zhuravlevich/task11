const Order = require("../models/order");
const Cart = require('../models/cart');
const User = require('../models/user');
// import { CartInterface } from "../models/cart";
import { UserInterface } from "../models/user";
import { OrderInterface } from "../models/order";
import { Request, Response } from "express";
let orderNumber = 1;

interface NewRequest<data> extends Request{
  body: data,
}


export class OrderController{
  public static async createOrder(req: NewRequest<OrderInterface>, res: Response): Promise<void> {
    try {
      let order = await Order.findOne({ number: orderNumber });
      let user = await User.findOne({ _id: (req as any).decoded.user_id });
      let cart = await Cart.findOne({ _id: user.cart._id });
      if (order) {
        res.status(400).send("Такой заказ уже существует");
      } else if (!cart) {
        res.status(400).send("Такой корзины не существует");
      } else {
        const newOrder = new Order({
          number: orderNumber,
          user: (req as any).decoded.user_id,
        });
        for (let i: number = 0; i < cart.products.length; i++) {
          newOrder.products.push(cart.products[i]);
        }
        await newOrder.save();
        user.orders.push(newOrder._id);
        await user.save();
        await User.updateOne(
          { cart: user.cart._id },
          { $unset: { cart: user.cart._id } }
        );
        await Cart.deleteOne({ _id: user.cart._id });
        orderNumber++;
        res.send(
          `Заказ сформирован ${newOrder.number}. Покупатель ${user.name}. Товары ${newOrder.products}`
        );
      }
    } catch (err) {
      res.status(400).send("Что-то пошло не так");
    }
  };
  
  public static async deleteOrder(req: NewRequest<UserInterface>, res: Response): Promise<void> {
    try {
        let user = await User.findOne({ _id: (req as any).decoded.user_id })
        if (user.role !== "admin") {
            let order = await Order.findById({ _id: req.params.id });
            for (let i: number = 0; i < user.orders.length; i++) {
                if (order._id.toString() == user.orders[i].toString()) {
                    let orderDel = await Order.findOneAndDelete({_id: req.params.id})
                    await User.updateOne({_id: (req as any).decoded.user_id}, { $pull: { orders:  req.params.id } })
                    res.status(400).send(`Заказ ${orderDel.number} удалён`)
                }
                else {
                    res.status(400).send("Такого заказа не существует или он не ваш")
                }
            }
        }
        else {
            let order = await Order.findById({ _id: req.params.id });
            if (!order) {
                res.status(400).send("Такого заказа не существует")
            }
            else {
                await User.updateOne({orders:order._id}, { $pull: { orders:  req.params.id } })
                let orderDel = await Order.findOneAndDelete({ _id: req.params.id })
                res.status(400).send(`Заказ ${orderDel.number} удалён`)
            }
        }
    }
    catch (err) {
        res.status(400).send("Что то пошло не так")
    }
  }
  
  
  public static async getOrderById(req: NewRequest<UserInterface>, res: Response): Promise<void> {
    try {
      let order = await Order.findOne({ _id: req.params.id });
      if (order) {
        res.send(`заказ: ${order}`);
      } else {
        res.send("Такого заказа не существует");
      }
    } catch (err) {
      res.status(400).send("Что-то пошло не так");
    }
  };
  
  public static async getAllOrders(req: NewRequest<UserInterface>, res: Response): Promise<void> {
      try {
          let user = await User.findOne({ _id: (req as any).decoded.user_id })
          if (user.role !== "Admin") {
              res.send(user.orders)
          }
          else {
              let orders = await Order.find();
              if (orders) {
                  res.send(`${orders}`)
              }
              else {
                  res.send("Заказы отсутствуют")
              }
          }
      }
      catch (err) {
          res.status(400).send("Что-то пошло не так")
      }
  };
};