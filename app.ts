import { Request, Response } from "express";
const express = (require("express"))
import mongoose from 'mongoose';
const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({extended: true});
const app = express();

const PORT = 8080;

const url = 'mongodb://localhost:27017/somedb';


import authRouter from "./routes/authRouter"
import userRouter from "./routes/userRouter"
import categoryRout from "./routes/categoryRout"
import productRout from "./routes/productRout"
import orderRout from "./routes/orderRout"
import cartRout from "./routes/cartRout"

// const homeRout = require('./routes/homeRout');
app.use('', urlencodedParser, authRouter);
app.use('/user', urlencodedParser, userRouter);
app.use('/category', urlencodedParser, categoryRout);
app.use('/product', urlencodedParser, productRout);
app.use('/cart', urlencodedParser, cartRout);
// app.use('/home', urlencodedParser, homeRout);
app.use('/order', urlencodedParser, orderRout);

// mongodb connection
(async()=>{
    try{await mongoose.connect(url,     
        {useNewUrlParser: true,
         useUnifiedTopology: true,
          useCreateIndex: true} as any)
        app.listen(PORT, () => console.log(`app running on port ${PORT}`))}
        catch (err){
            console.log(err);
        }
        })();

app.get('/', (req: Request, res: Response) => {
    res.setHeader('Content-Type', 'text/html')
    res.end('<h1>Hello TypeScript</h1>')
});

